#ifndef PROG_VALENCE_DE
#define PROG_VALENCE_DE

#define TRISUB 0
#define TRIADD 1
#define TRIINIT 2

#define FREE 0
#define CONQUERED 1
#define REMOVABLE 2

//Retriangulation flag tables
int retri_flag_tri[2][2] = {{TRIADD, //--
                        TRIADD}, //-+
                        {TRIADD, //+-
                        TRISUB}}; //++

int retri_flag_quad[2][2][2] = {{{TRIADD, TRISUB}, //--
                            {TRISUB, TRIADD}}, //-+
                            {{TRIADD, TRISUB}, //+-
                            {TRISUB, TRIADD}}}; //++

int retri_flag_pent[2][2][3] = {{{TRIADD, TRISUB, TRIADD}, //-- 
                            {TRIADD, TRISUB, TRIADD}}, //-+
                            {{TRIADD, TRISUB, TRIADD}, //+-
                            {TRISUB, TRIADD, TRISUB}}}; //++

int retri_flag_hex[2][2][4] = {{{TRIADD, TRISUB, TRIADD, TRISUB}, //--
                           {TRISUB, TRIADD, TRISUB, TRIADD}}, //-+
                           {{TRIADD, TRISUB, TRIADD, TRISUB}, //+-
                           {TRISUB, TRIADD, TRISUB, TRIADD}}}; //++

//Retriangulation configuration tables
//Contains indexes of vertices starting with left vertex in gate as 0.
int retri_conf_quad[2][2][2][3] = {{{{0, 1, 2}, {0, 2, 3}}, //--
                            {{0, 1, 3}, {1, 2, 3}}}, //-+
                            {{{0, 1, 2}, {0, 2, 3}}, //+-
                            {{0, 1, 3}, {1, 2, 3}}}}; //++

int retri_conf_pent[2][2][3][3] = {{{{0, 1, 2}, {0, 2, 4}, {2, 3, 4}}, //-- 
                            {{0, 1, 4}, {1, 2, 4}, {2, 3, 4}}}, //-+
                            {{{0, 1, 2}, {0, 2, 4}, {2, 3, 4}}, //+-
                            {{0, 1, 3}, {1, 2, 3}, {0, 3, 4}}}}; //++

int retri_conf_hex[2][2][4][3] = {{{{0, 2, 4}, {0, 1, 2}, {2, 3, 4}, {4, 5, 0}}, //--
                           {{0, 1, 5}, {1, 3, 5}, {1, 2, 3}, {3, 4, 5}}}, //-+
                           {{{0, 2, 4}, {0, 1, 2}, {2, 3, 4}, {4, 5, 0}}, //+-
                           {{0, 1, 5}, {1, 3, 5}, {1, 2, 3}, {3, 4, 5}}}}; //++


//Indices in patch_vertices in insert_general_vertex() used to find the faces and vertices in the 
//patches retriangulated in the encoding step.
int repatch_conf_quad[2][2][2][2] = {{{{0, 1}, {0, 2}}, //--
                            {{0, 1}, {1, 2}}}, //-+
                            {{{0, 1}, {0, 2}}, //+-
                            {{0, 1}, {1, 2}}}}; //++

int repatch_conf_pent[2][2][3][2] = {{{{0,1}, {0, 2}, {2, 3}}, //-- 
                            {{0, 1}, {1, 2}, {2, 3}}}, //-+
                            {{{0,1}, {0, 2}, {2, 3}}, //+-
                            {{0, 1}, {0, 2}, {1, 2}}}}; //++

int repatch_conf_hex[2][2][4][2] = {{{{0, 1}, {0, 2}, {0, 3}, {2, 3}}, //--
                           {{0, 1}, {1, 2}, {2, 3}, {1, 3}}}, //-+
                           {{{0, 1}, {0, 2}, {0, 3}, {2, 3}}, //+-
                           {{0, 1}, {1, 2}, {2, 3}, {1, 3}}}}; //++

//Contains a series of indices used to get vertices in patch_vertices in 
//insert_general_vertex() in counter clockwise order.
int ccw_vertices_tri[2][2][3] = {{{0, 1, 2}, //--
                           {0, 1, 2}}, //-+
                           {{0, 1, 2}, //+-
                           {0, 1, 2}}}; //++

int ccw_vertices_quad[2][2][4] = {{{0, 1, 2, 3}, //--
                           {0, 1, 3, 2}}, //-+
                           {{0, 1, 2, 3}, //+-
                           {0, 1, 3, 2}}}; //++

int ccw_vertices_pent[2][2][5] = {{{0, 1, 2, 4, 3}, //--
                           {0, 1, 3, 4, 2}}, //-+
                           {{0, 1, 2, 4, 3}, //+-
                           {0, 1, 4, 2, 3}}}; //++

int ccw_vertices_hex[2][2][6] = {{{0, 1, 2, 5, 3, 4}, //--
                           {0, 1, 5, 3, 4, 2}}, //-+
                           {{0, 1, 2, 5, 3, 4}, //+-
                           {0, 1, 5, 3, 4, 2}}}; //++

#endif
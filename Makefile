CC = g++
INCLUDE = -I/home/atle/Documents/Master/Code/OpenMesh-4.1/src/
DIR = -L/home/atle/Documents/Master/Code/OpenMesh-4.1/build/Build/lib/
LIB = -lOpenMeshCore -lOpenMeshTools

all: valence_encoder valence_decoder

valence_encoder: valence_encoder.cc
	$(CC) -o valence_encoder valence_encoder.cc $(INCLUDE) $(DIR) $(LIB)

valence_decoder: valence_decoder.cc
	$(CC) -o valence_decoder valence_decoder.cc $(INCLUDE) $(DIR) $(LIB)


encode:
	./valence_encoder mesh.off output.off
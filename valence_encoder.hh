#ifndef VALENCE_CODER
#define VALENCE_CODER

#define TRISUB 0
#define TRIADD 1
#define TRIINIT 2

#define FREE 0
#define CONQUERED 1
#define REMOVABLE 2

//Retriangulation flag tables
int retri_flag_tri[2][2] = {{TRIADD, //--
                        TRIADD}, //-+
                        {TRIADD, //+-
                        TRISUB}}; //++

int retri_flag_quad[2][2][2] = {{{TRIADD, TRISUB}, //--
                            {TRISUB, TRIADD}}, //-+
                            {{TRIADD, TRISUB}, //+-
                            {TRISUB, TRIADD}}}; //++

int retri_flag_pent[2][2][3] = {{{TRIADD, TRISUB, TRIADD}, //-- 
                            {TRIADD, TRISUB, TRIADD}}, //-+
                            {{TRIADD, TRISUB, TRIADD}, //+-
                            {TRISUB, TRIADD, TRISUB}}}; //++

int retri_flag_hex[2][2][4] = {{{TRIADD, TRISUB, TRIADD, TRISUB}, //--
                           {TRISUB, TRIADD, TRISUB, TRIADD}}, //-+
                           {{TRIADD, TRISUB, TRIADD, TRISUB}, //+-
                           {TRISUB, TRIADD, TRISUB, TRIADD}}}; //++

//Retriangulation configuration tables
//Contains indexes of vertices starting with left vertex in gate as 0.
int retri_conf_quad[2][2][2][3] = {{{{0, 1, 2}, {0, 2, 3}}, //--
                            {{0, 1, 3}, {1, 2, 3}}}, //-+
                            {{{0, 1, 2}, {0, 2, 3}}, //+-
                            {{0, 1, 3}, {1, 2, 3}}}}; //++

int retri_conf_pent[2][2][3][3] = {{{{0, 1, 2}, {0, 2, 4}, {2, 3, 4}}, //-- 
                            {{0, 1, 4}, {1, 2, 4}, {2, 3, 4}}}, //-+
                            {{{0, 1, 2}, {0, 2, 4}, {2, 3, 4}}, //+-
                            {{0, 1, 3}, {1, 2, 3}, {0, 3, 4}}}}; //++

int retri_conf_hex[2][2][4][3] = {{{{0, 2, 4}, {0, 1, 2}, {2, 3, 4}, {4, 5, 0}}, //--
                           {{0, 1, 5}, {1, 3, 5}, {1, 2, 3}, {3, 4, 5}}}, //-+
                           {{{0, 2, 4}, {0, 1, 2}, {2, 3, 4}, {4, 5, 0}}, //+-
                           {{0, 1, 5}, {1, 3, 5}, {1, 2, 3}, {3, 4, 5}}}}; //++

#endif
#include <iostream>
#include <stdint.h>
#include <fstream>
#include <queue>
// -------------------- OpenMesh
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Tools/Utils/getopt.h>
#include <OpenMesh/Core/System/omstream.hh>
// ----------------------------------------------------------------------------
#include "valence_decoder.hh"

typedef OpenMesh::TriMesh_ArrayKernelT<>  MyMesh;

MyMesh mesh;
// State flags used in encoding of the mesh.
OpenMesh::FPropHandleT<MyMesh::Scalar> f_state_flag;
OpenMesh::HPropHandleT<MyMesh::Scalar> h_state_flag;
OpenMesh::VPropHandleT<MyMesh::Scalar> v_state_flag;

// Flag used for retriangulation
OpenMesh::VPropHandleT<MyMesh::Scalar> v_retri_flag;

std::deque<MyMesh::HalfedgeHandle> gate_queue;

char* encoded_data;
size_t encoded_data_index;

MyMesh::Point read_point(){
    MyMesh::Point point;

    int i;
    for(i = 0; i < 3; i++) {
        int16_t* coordinate = (int16_t*) &(encoded_data[encoded_data_index]);
        point[i] = *coordinate;
        encoded_data_index += 2;
    }

    return point;
}

uint8_t read_valence() {

    uint8_t encoded_char = encoded_data[encoded_data_index];
    encoded_data_index++;
    return encoded_char;
}

void initialize_mesh_data() {
    std::cout << "Initializing vertex data.\n";
    MyMesh::VertexIter v_it, v_end(mesh.vertices_end());
    for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it) {
        //Set triangulation flag to 3
        mesh.property(v_retri_flag, *v_it) = TRIINIT;
        mesh.property(v_state_flag, *v_it) = FREE;
    }

    std::cout << "Initializing face data.\n";
    MyMesh::FaceIter f_it, f_end(mesh.faces_end());
    for (f_it=mesh.faces_begin(); f_it!=f_end; ++f_it) {
        mesh.property(f_state_flag, *f_it) = FREE;
    }  

    gate_queue.clear();
}

void print_mesh_data() {
    std::cout << "Vertices:\n";
    MyMesh::VertexIter v_it, v_end(mesh.vertices_end());
    for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it) {
        std::cout << *v_it << " " << mesh.property(v_state_flag, *v_it) << "\n";
    }
    std::cout << "Faces:\n";
    MyMesh::FaceIter f_it, f_end(mesh.faces_end());
    for (f_it=mesh.faces_begin(); f_it!=f_end; ++f_it) {
        std::cout << *f_it << " " << mesh.property(f_state_flag, *f_it) << "\n";
    }
}


void debug_print_gate(MyMesh::HalfedgeHandle heh) {
    MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(heh);
    MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(heh);

    std::cout << "(" << heh << ") ";
    std::cout << from_vertex << "-" << to_vertex;
    std::cout << " " << mesh.opposite_vh(heh);

    std::cout << " TRIFLAGS: " << mesh.property(v_retri_flag, from_vertex);
    std::cout << mesh.property(v_retri_flag, to_vertex) << "\n";
}

void debug_print_deque(std::deque<MyMesh::HalfedgeHandle> debug_queue) {
    //DEBUG
    //Print and empty queue
    std::cout << "Deque:\n";
    for (int i=0; i<(signed)debug_queue.size(); ++i) {

        MyMesh::HalfedgeHandle heh = debug_queue.at(i);

        MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(heh);
        MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(heh);

        std::cout << from_vertex << "-" << to_vertex;
        std::cout << " " << mesh.opposite_vh(heh);

        std::cout << " TRIFLAGS: " << mesh.property(v_retri_flag, from_vertex);
        std::cout << mesh.property(v_retri_flag, to_vertex);

        std::cout  << "\n";
    } 

    std::cout << "\n";
}

void empty_general_patch(MyMesh::VertexHandle patch_vertices[], 
                                int valence_code,
                                MyMesh::HalfedgeHandle current_gate) {
    //Deletes faces in patch
    MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(current_gate);
    MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(current_gate);

    int from_flag = mesh.property(v_retri_flag, from_vertex);
    int to_flag = mesh.property(v_retri_flag, to_vertex);

    MyMesh::FaceHandle patch_faces[valence_code-2];
    
    int i;
    //Find valence_code-2 faces
    for(i = 0; i < valence_code-2; i++) {
        //Find correct vertices according to configuration tables.
        if (valence_code == 3) {
            
        }
        else if (valence_code == 4) {
            from_vertex = patch_vertices[repatch_conf_quad[from_flag][to_flag][i][0]];
            to_vertex = patch_vertices[repatch_conf_quad[from_flag][to_flag][i][1]];
        }
        else if (valence_code == 5) {
            from_vertex = patch_vertices[repatch_conf_pent[from_flag][to_flag][i][0]];
            to_vertex = patch_vertices[repatch_conf_pent[from_flag][to_flag][i][1]];
        }
        else if (valence_code == 6) {
            from_vertex = patch_vertices[repatch_conf_hex[from_flag][to_flag][i][0]];
            to_vertex = patch_vertices[repatch_conf_hex[from_flag][to_flag][i][1]];
        }
            
        MyMesh::HalfedgeHandle gate;

        //Find correct edge according to configuration tables.
        //Use edge to find next face in patch.
        MyMesh::VertexOHalfedgeIter voh_it;
        for (voh_it=mesh.voh_iter(from_vertex); voh_it.is_valid(); ++voh_it) {
            if(mesh.to_vertex_handle(*voh_it) == to_vertex) {
                gate = *voh_it;

                //In case the halfedge is adjecent to an already visited face
                int face_flag = mesh.property(f_state_flag, 
                                             mesh.face_handle(gate));
                if(face_flag == REMOVABLE) {
                    gate = mesh.opposite_halfedge_handle(gate);
                }
                break;
            }
        }

        MyMesh::FaceHandle face = mesh.face_handle(gate);
        patch_faces[i] = face;
        std::cout << "Face set to be deleted " << i << ": " << patch_faces[i] << "\n";
        mesh.property(f_state_flag, face) = REMOVABLE;

        //Set next vertex to be processed in for loop
        patch_vertices[i+2] = mesh.opposite_vh(gate);
    }

    //Delete valence_code-2 faces
    for(i = 0; i < valence_code-2; i++) {
        mesh.delete_face(patch_faces[i]);
        std::cout << patch_faces[i] << " deleted \n";
    }

    std::cout << "Faces has been deleted\n";

}

void fill_general_patch(MyMesh::VertexHandle patch_vertices[], 
                                int valence_code,
                                MyMesh::HalfedgeHandle current_gate,
                                MyMesh::Point vertex_point) {
    int i;

    MyMesh::Point barycenter_sum;
    barycenter_sum[0] = barycenter_sum[1] = barycenter_sum[2] = 0.0;
    for (i = 0; i < valence_code; i++) {
        barycenter_sum += mesh.point(patch_vertices[i]);
    }
    MyMesh::Point barycenter = barycenter_sum/valence_code;
    MyMesh::Point real_vertex_point = barycenter-vertex_point;

    std::cout << "Barycenter: " << barycenter << "\n";
    std::cout << "Difference: " << vertex_point << "\n";
    std::cout << "Real vertex point: " << real_vertex_point << "\n";

    //Add new vertex in patch
    MyMesh::VertexHandle new_vertex = mesh.add_vertex(real_vertex_point);
    mesh.property(v_state_flag, new_vertex) = CONQUERED;

    //Add new faces
    MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(current_gate);
    MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(current_gate);

    int from_flag = mesh.property(v_retri_flag, from_vertex);
    int to_flag = mesh.property(v_retri_flag, to_vertex);

    for(i = 0; i < valence_code; i++) {
        if (valence_code == 3) {
            from_vertex = patch_vertices[ccw_vertices_tri[from_flag][to_flag][i]];
            if(i == valence_code-1) {
                to_vertex = patch_vertices[ccw_vertices_tri[from_flag][to_flag][0]];
            }
            else {
                to_vertex = patch_vertices[ccw_vertices_tri[from_flag][to_flag][i+1]];
            }
        }
        else if (valence_code == 4) {
            from_vertex = patch_vertices[ccw_vertices_quad[from_flag][to_flag][i]];
            if(i == valence_code-1) {
                to_vertex = patch_vertices[ccw_vertices_quad[from_flag][to_flag][0]];
            }
            else {
                to_vertex = patch_vertices[ccw_vertices_quad[from_flag][to_flag][i+1]];
            }
        }
        else if (valence_code == 5) {
            from_vertex = patch_vertices[ccw_vertices_pent[from_flag][to_flag][i]];
            if(i == valence_code-1) {
                to_vertex = patch_vertices[ccw_vertices_pent[from_flag][to_flag][0]];
            }
            else {
                to_vertex = patch_vertices[ccw_vertices_pent[from_flag][to_flag][i+1]];
            }
        }
        else if (valence_code == 6) {
            from_vertex = patch_vertices[ccw_vertices_hex[from_flag][to_flag][i]];
            if(i == valence_code-1) {
                to_vertex = patch_vertices[ccw_vertices_hex[from_flag][to_flag][0]];
            }
            else {
                to_vertex = patch_vertices[ccw_vertices_hex[from_flag][to_flag][i+1]];
            }
        }

        //Set retriangulation flags
        if(i > 0 && i < valence_code-1) {
            if(mesh.property(v_retri_flag, to_vertex) != TRIINIT) {
                //Do nothing
                std::cout << "Vertex retri flags already set\n";
            }
            else if(valence_code == 3) {
                mesh.property(v_retri_flag, to_vertex) = retri_flag_tri[from_flag][to_flag];
            }
            else if(valence_code == 4) {
                mesh.property(v_retri_flag, to_vertex) = retri_flag_quad[from_flag][to_flag][i-1];
            }
            else if(valence_code == 5) {
                mesh.property(v_retri_flag, to_vertex) = retri_flag_pent[from_flag][to_flag][i-1];
            }
            else if(valence_code == 6) {
                mesh.property(v_retri_flag, to_vertex) = retri_flag_hex[from_flag][to_flag][i-1];
            }

            std::cout << "i: " << i << ": "
                      << "Set vertex " << to_vertex
                      << " retro_flag to " 
                      << mesh.property(v_retri_flag, to_vertex) << "\n";
        }

        //Create face, and flag it as conquered
        std::vector<MyMesh::VertexHandle> face_vhandles;
        face_vhandles.clear();
        face_vhandles.push_back(from_vertex);
        face_vhandles.push_back(to_vertex);
        face_vhandles.push_back(new_vertex);
        MyMesh::FaceHandle new_face = mesh.add_face(face_vhandles);
        mesh.property(f_state_flag, new_face) = CONQUERED;

        std::cout << "Create new face " << new_face << "\n";

        if(i > 0) {
            //Add gate to queue
            MyMesh::VertexOHalfedgeIter voh_it;
            for (voh_it=mesh.voh_iter(to_vertex); voh_it.is_valid(); ++voh_it) {
                if(mesh.to_vertex_handle(*voh_it) == from_vertex) {
                    gate_queue.push_back(*voh_it);

                    std::cout << "Pushed gate: " 
                              << mesh.from_vertex_handle(*voh_it) << "-"
                              << mesh.to_vertex_handle(*voh_it) << "\n";
                    break;
                }
            } 
        }
    }
}

void insert_general_vertex(MyMesh::HalfedgeHandle current_gate, 
                   int valence_code,
                   MyMesh::Point vertex_point) {

    //DEBUG
    MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(current_gate);
    MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(current_gate);
    int from_flag = mesh.property(v_retri_flag, from_vertex);
    int to_flag = mesh.property(v_retri_flag, to_vertex);

    std::cout << "insert_general_vertex (v" << valence_code << "): gate flags: \n";
    std::cout << "    from vertex (" << from_vertex << "): " << from_flag << "\n"; 
    std::cout << "    to vertex (" << to_vertex << "): " << to_flag << "\n"; 
    //DEBUG END

    MyMesh::VertexHandle patch_vertices[valence_code];
    patch_vertices[0] = from_vertex;
    patch_vertices[1] = to_vertex;

    //Delete faces in patch
    empty_general_patch(patch_vertices, valence_code, current_gate);
    
    //Insert vertex and faces in patch
    fill_general_patch(patch_vertices, valence_code, current_gate, vertex_point);

    //DEBUG
    int i, j;
    std::cout << "Vertices in patch_vertices\n";
    for(i = 0; i < valence_code; i++) {
        std::cout << patch_vertices[i].idx() << " ";
    }
    std::cout << "\n";

    std::cout << "Vertices in patch_vertices in ccw order\n";
    for(i = 0; i < valence_code; i++) {
        if(valence_code == 3) {
            std::cout << patch_vertices[ccw_vertices_tri[from_flag][to_flag][i]] << " ";
        }
        if(valence_code == 4) {
            std::cout << patch_vertices[ccw_vertices_quad[from_flag][to_flag][i]] << " ";
        }
        if(valence_code == 5) {
            std::cout << patch_vertices[ccw_vertices_pent[from_flag][to_flag][i]] << " ";
        }
        if(valence_code == 6) {
            std::cout << patch_vertices[ccw_vertices_hex[from_flag][to_flag][i]] << " ";
        }
    }
    std::cout << "\n";

    std::cout << "retri flags in patch_vertices in ccw order\n";
    for(i = 0; i < valence_code; i++) {
        if(valence_code == 3) {
            std::cout << mesh.property(v_retri_flag, patch_vertices[ccw_vertices_tri[from_flag][to_flag][i]]) << " ";
        }
        if(valence_code == 4) {
            std::cout << mesh.property(v_retri_flag, patch_vertices[ccw_vertices_quad[from_flag][to_flag][i]]) << " ";
        }
        if(valence_code == 5) {
            std::cout << mesh.property(v_retri_flag, patch_vertices[ccw_vertices_pent[from_flag][to_flag][i]]) << " ";
        }
        if(valence_code == 6) {
            std::cout << mesh.property(v_retri_flag, patch_vertices[ccw_vertices_hex[from_flag][to_flag][i]]) << " ";
        }
    }
    std::cout << "\n";
    std::cout << "Ending insert_general_vertex\n";
    //DEBUG END
}

void insert_cleaning_vertex(MyMesh::HalfedgeHandle current_gate,
                            MyMesh::Point vertex_point) {

    int valence_code = 3;
    MyMesh::FaceHandle face = mesh.face_handle(current_gate);

    MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(current_gate);
    MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(current_gate);

    int from_flag = mesh.property(v_retri_flag, from_vertex);
    int to_flag = mesh.property(v_retri_flag, to_vertex);

    std::cout << "insert_cleaning_vertex (v" << valence_code << "): gate flags: \n";
    std::cout << "    from vertex (" << from_vertex << "): " << from_flag << "\n"; 
    std::cout << "    to vertex (" << to_vertex << "): " << to_flag << "\n"; 

    MyMesh::VertexHandle patch_vertices[valence_code];
    patch_vertices[0] = from_vertex;
    patch_vertices[1] = to_vertex;
    patch_vertices[2] = mesh.opposite_vh(current_gate);

    MyMesh::FaceHandle patch_faces[valence_code-2];

    MyMesh::VertexOHalfedgeIter voh_it;
    int i, j;

    //Delete face
    mesh.delete_face(face);
    std::cout << "Face " << face << " deleted \n";

    //Add new vertex
    MyMesh::VertexHandle new_vertex = mesh.add_vertex(vertex_point);
    mesh.property(v_state_flag, new_vertex) = CONQUERED;

    //Add new faces
    for(i = 0; i < valence_code; i++) {
        from_vertex = patch_vertices[i];
        if(i == valence_code-1) {
            to_vertex = patch_vertices[0];
        }
        else {
            to_vertex = patch_vertices[i+1];
        }

        //Create face, and flag it as conquered
        std::vector<MyMesh::VertexHandle> face_vhandles;
        face_vhandles.clear();
        face_vhandles.push_back(from_vertex);
        face_vhandles.push_back(to_vertex);
        face_vhandles.push_back(new_vertex);
        MyMesh::FaceHandle new_face = mesh.add_face(face_vhandles);
        mesh.property(f_state_flag, new_face) = CONQUERED;

        std::cout << "Create new face " << new_face << "\n";

        //Add gates to queue if not working on current_gates face
        if(i > 0) {
            for (voh_it=mesh.voh_iter(to_vertex); voh_it.is_valid(); ++voh_it) {
                if(mesh.to_vertex_handle(*voh_it) == from_vertex) {

                    mesh.property(v_state_flag, mesh.opposite_vh(*voh_it)) = CONQUERED;
                    std::cout << "Vertex " <<
                                 mesh.opposite_vh(*voh_it) <<
                                 " flagged conquered\n"; 

                    //Flag adjecent face as conquered
                    mesh.property(f_state_flag, 
                                  mesh.face_handle(*voh_it)) = CONQUERED;
                    std::cout << "Face "  <<
                                 mesh.face_handle(*voh_it) <<
                                 " flagged conquered\n";

                    //Add first gate
                    MyMesh::HalfedgeHandle next_heh = mesh.next_halfedge_handle(*voh_it);
                    MyMesh::HalfedgeHandle next_gate = mesh.opposite_halfedge_handle(next_heh);
                    gate_queue.push_back(next_gate);
                    std::cout << "Pushed gate ";
                    debug_print_gate(next_gate);

                    //Add second gate
                    next_heh = mesh.next_halfedge_handle(next_heh);
                    next_gate = mesh.opposite_halfedge_handle(next_heh);
                    gate_queue.push_back(next_gate);
                    std::cout << "Pushed gate ";
                    debug_print_gate(next_gate);

                    gate_queue.push_back(next_gate);

                    break;
                }
            } 
        }
    }

    //DEBUG
    std::cout << "Vertices in patch_vertices\n";
    for(i = 0; i < valence_code; i++) {
        std::cout << patch_vertices[i].idx() << " ";
    }
    std::cout << "\n";

    std::cout << "Vertices in patch_vertices in ccw order\n";
    for(i = 0; i < valence_code; i++) {
        std::cout << patch_vertices[ccw_vertices_tri[from_flag][to_flag][i]] << " ";
    }
    std::cout << "\n";

    std::cout << "retri flags in patch_vertices in ccw order\n";
    for(i = 0; i < valence_code; i++) {
        std::cout << mesh.property(v_retri_flag, patch_vertices[ccw_vertices_tri[from_flag][to_flag][i]]) << " ";
    }
    std::cout << "\n";

    std::cout << "Ending insert_cleaning_vertex\n";

}

void process_null_patch(MyMesh::FaceHandle null_face, 
                        MyMesh::HalfedgeHandle current_gate) {
    MyMesh::FaceHalfedgeIter fh_it;
    for (fh_it=mesh.fh_iter(null_face); fh_it.is_valid(); ++fh_it) {
        if(*fh_it != current_gate) {
            MyMesh::HalfedgeHandle next_gate = mesh.opposite_halfedge_handle(*fh_it);
            gate_queue.push_back(next_gate);

            //Flag vertices as conquered if flag is not all ready set
            MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(next_gate);
            MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(next_gate);

            if (mesh.property(v_retri_flag, from_vertex) == TRIINIT) {
                mesh.property(v_retri_flag, from_vertex) = TRIADD;
            }
            if (mesh.property(v_retri_flag, to_vertex) == TRIINIT) {
                mesh.property(v_retri_flag, to_vertex) = TRISUB;
            }

            std::cout << "Null patch, pushed gate: " << 
                         mesh.from_vertex_handle(next_gate) << "-" <<
                         mesh.to_vertex_handle(next_gate) << "\n";
        }
    }
}

void add_layer(char* encoded_file_name, int cleaning) {
    MyMesh::VertexVertexCCWIter vvccw_it;
    MyMesh::VertexFaceCCWIter vfccw_it;
    MyMesh::FaceHalfedgeIter fh_it;
    MyMesh::VertexOHalfedgeIter voh_it;
    
    std::ifstream inFile;
    size_t encoded_data_size = 0;
    int d_break = 0;

    //Open encoded file
    inFile.open( encoded_file_name, std::ios::in|std::ios::binary|std::ios::ate );
    inFile.seekg(0, std::ios::end); 
    encoded_data_size = inFile.tellg();
    std::cout << "size of file: " << encoded_data_size << "\n";
    inFile.seekg(0, std::ios::beg);

    //Read encoded file
    encoded_data = new char[encoded_data_size];
    inFile.read( encoded_data, encoded_data_size );
    encoded_data_index = 0;
    
    //Find starting gate
    MyMesh::HalfedgeHandle init_heh;
    MyMesh::VertexHandle first_vertex = *mesh.vertices_begin();
    
    int lowest_idx = -1;
    for (voh_it=mesh.voh_iter(first_vertex); voh_it.is_valid(); ++voh_it) {
        if(lowest_idx == -1 ||
            mesh.to_vertex_handle(*voh_it).idx() < lowest_idx) {
            init_heh = *voh_it;
            lowest_idx = mesh.to_vertex_handle(*voh_it).idx();
        }
    }
    gate_queue.push_back(init_heh);

    //Flag its vertices as conquered.
    mesh.property(v_state_flag, mesh.from_vertex_handle(init_heh)) = CONQUERED;
    mesh.property(v_state_flag, mesh.to_vertex_handle(init_heh)) = CONQUERED;

    //Set retriangulation flags
    mesh.property(v_retri_flag, mesh.from_vertex_handle(init_heh)) = TRISUB;
    mesh.property(v_retri_flag, mesh.to_vertex_handle(init_heh)) = TRIADD;

    //Improve mesh until encoded file is empty
    while (encoded_data_index < encoded_data_size) {
        MyMesh::HalfedgeHandle current_gate = gate_queue.front();
        MyMesh::FaceHandle face = mesh.face_handle(current_gate);

        std::cout << "\nloop: " << d_break << " Started working on gate " 
                  << mesh.from_vertex_handle(current_gate) << "-"
                  << mesh.to_vertex_handle(current_gate) << "\n";

        if(mesh.property(f_state_flag, face) == CONQUERED ||
           mesh.property(f_state_flag, face) == REMOVABLE) {
            //Nothing to do, do not process a patch.

            //DEBUG
            if (mesh.property(f_state_flag, face) == CONQUERED) {
                std::cout << "Ignoring conquered face: " << face << "\n";
            }
            else if(mesh.property(f_state_flag, face) == REMOVABLE) {
                std::cout << "Ignoring removable face: " << face << "\n";
            }

            //Get gate
            std::cout << "Popping gate\n";
            gate_queue.pop_front();
            d_break++;
            continue;
        }

        int valence_code = read_valence();
        if(valence_code != 0) {
            MyMesh::Point vertex_point = read_point();
            std::cout << valence_code << " " << vertex_point << "\n";

            //Insert vertex
            if(cleaning) {
                insert_cleaning_vertex(current_gate, vertex_point);
            }
            else {
                insert_general_vertex(current_gate, valence_code, vertex_point);
            }
        }
        else {
            mesh.property(f_state_flag, face) = CONQUERED;
            std::cout << "Null patch, detection: face " << face <<"!\n";

            process_null_patch(face, current_gate);
        }
        std::cout << "Popping gate\n";
        gate_queue.pop_front();
        // debug_print_deque(gate_queue);

        // DEBUG
        if(!cleaning && d_break >= 5739) { //Crash at 5740 (cow, general conquest)
            // break;
        }
        d_break++;
    }

    // Delete all elements that are marked as deleted by OpenMesh.
    mesh.garbage_collection();
}

int main(int argc, char* argv[]) {
    /*
    Usage:

    prog_valence_de base_mesh output_mesh [cleaning_layer general_layer]...
    */

    mesh.add_property(f_state_flag);
    mesh.add_property(h_state_flag);
    mesh.add_property(v_state_flag);
    mesh.add_property(v_retri_flag);

    //Enabled deletion of mesh items
    mesh.request_face_status();
    mesh.request_edge_status();

    // Read base mesh
    if (!OpenMesh::IO::read_mesh(mesh, argv[1])) 
    {
        std::cerr << "read error\n";
        exit(1);
    }

    //Add cleaning layer from file
    initialize_mesh_data();
    add_layer(argv[3], true);

    //Add general layer from file
    // initialize_mesh_data();
    // add_layer(argv[4], false);

    // Write output mesh
    if (!OpenMesh::IO::write_mesh(mesh, argv[2])) 
    {
        std::cerr << "write error\n";
        exit(1);
    }


    return 0;
}
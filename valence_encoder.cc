#include <iostream>
#include <stdint.h>
#include <math.h>
#include <fstream>
#include <queue>
#include <bitset>
#include <algorithm>
// -------------------- OpenMesh
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Tools/Utils/getopt.h>

// #include <OpenMesh/Core/IO/reader/PLYReader.hh>
#include <OpenMesh/Core/System/omstream.hh>
// ----------------------------------------------------------------------------
#include "valence_encoder.hh"

typedef OpenMesh::TriMesh_ArrayKernelT<>  MyMesh;

MyMesh mesh;
// State flags used in encoding of the mesh.
OpenMesh::FPropHandleT<MyMesh::Scalar> f_state_flag;
OpenMesh::HPropHandleT<MyMesh::Scalar> h_state_flag;
OpenMesh::VPropHandleT<MyMesh::Scalar> v_state_flag;

// Flag used for retriangulation
OpenMesh::VPropHandleT<MyMesh::Scalar> v_retri_flag;

std::deque<MyMesh::HalfedgeHandle> removable_gates;
std::deque<MyMesh::HalfedgeHandle> gate_queue;

std::ofstream compressed_file;

MyMesh::Color red_color;

char bit_buffer;
int bit_buffer_len;


void debug_print_patch(MyMesh::HalfedgeHandle gate) {
    MyMesh::FaceHandle face = mesh.face_handle(gate);
    MyMesh::VertexHandle front_vertex = mesh.opposite_vh(gate);
    uint8_t patch_degree = mesh.valence(front_vertex);

    std::cout << "Front vertex: " << front_vertex << "("
              << mesh.point(front_vertex) << ") with valence " << int(patch_degree) << "\n";

    std::cout << "Neighborhood vertices:\n";
    MyMesh::VertexVertexCCWIter vvccw_it;
    for (vvccw_it=mesh.vv_ccwiter(front_vertex); vvccw_it.is_valid(); ++vvccw_it) {
        std::cout << (*vvccw_it) << " (" << mesh.point(*vvccw_it) << ") "
                  << "state: " << mesh.property(v_state_flag, *vvccw_it) 
                  << " retri: " << mesh.property(v_retri_flag, *vvccw_it) << "\n";
    }
    std::cout << "Neighborhood faces:\n";
    MyMesh::VertexFaceCCWIter vfccw_it;
    for (vfccw_it=mesh.vf_ccwiter(front_vertex); vfccw_it.is_valid(); ++vfccw_it) {
        std::cout << (*vfccw_it) << " state: "
                  << mesh.property(f_state_flag, *vfccw_it) << "\n";
    }
}

void debug_print_deque(std::deque<MyMesh::HalfedgeHandle> debug_queue) {
    //DEBUG
    //Print and empty queue
    std::cout << "Deque:\n";
    for (int i=0; i<(signed)debug_queue.size(); ++i) {

        MyMesh::HalfedgeHandle heh = debug_queue.at(i);

        MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(heh);
        MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(heh);

        std::cout << from_vertex << "-" << to_vertex;
        std::cout << " " << mesh.opposite_vh(heh);

        std::cout << " TRIFLAGS: " << mesh.property(v_retri_flag, from_vertex);
        std::cout << mesh.property(v_retri_flag, to_vertex);

        std::cout  << "\n";
    } 

    std::cout << "\n";
}

void debug_print_gate(MyMesh::HalfedgeHandle heh) {
    MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(heh);
    MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(heh);

    std::cout << "(" << heh << ") ";
    std::cout << from_vertex << "-" << to_vertex;
    std::cout << " " << mesh.opposite_vh(heh);

    std::cout << " TRIFLAGS: " << mesh.property(v_retri_flag, from_vertex);
    std::cout << mesh.property(v_retri_flag, to_vertex) << "\n";

}

void debug_print_face(MyMesh::FaceHandle face) {
    std::cout << "face " << face << " ";
    MyMesh::FaceHalfedgeIter fh_it;
    for (fh_it=mesh.fh_iter(face); fh_it.is_valid(); ++fh_it) {

        MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(*fh_it);
        MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(*fh_it);
        
        std::cout << "(" << from_vertex << ": " << mesh.point(from_vertex) << ") ";
    }
    std::cout << "\n";
}

void write_bits(char* message, int num_bits) {

    int i = 0;

    std::bitset<8> mesbit(message[0]);
    std::cout << "Message bits: " << mesbit 
              << " Bits to write: " << num_bits << "\n";

    while(i < num_bits) {
        int remaining_bits = num_bits-i;

        if (bit_buffer_len < 8 && remaining_bits > 0){
            //Get free buffer space
            int buffer_space = 8-bit_buffer_len;

            //Get number of bits to copy from message
            int bit_count = 8-(i%8);
            if(remaining_bits < bit_count) {
                bit_count = remaining_bits;
            }
            if(buffer_space < bit_count) {
                bit_count = buffer_space;
            }

            // Mask specifying number of bits to copy from byte
            uint8_t bit_mask = pow(2, bit_count)-1;

            // Mask specifying which bits to copy from byte
            uint8_t copy_bit_mask = bit_mask<<(8-(i%8)-bit_count);

            // Copied bits without leading zeroes
            uint8_t copy_bits = (message[i/8] & copy_bit_mask)<<(i%8);

            std::bitset<8> leftor(bit_buffer);
            std::bitset<8> rightor(copy_bits>>bit_buffer_len);
            
            // Add copied bits to end of buffer byte
            bit_buffer = bit_buffer | (copy_bits>>bit_buffer_len);
            

            std::cout << "Bit count: " << bit_count << "\n";
            std::bitset<8> copymask(copy_bit_mask);
            std::cout << "copy bit mask: " << copymask << "\n";
            std::bitset<8> copied_bits(copy_bits);
            std::cout << "copied bits: " << copied_bits << "\n";
            std::bitset<8> bufaft(bit_buffer);
            std::cout << "Or: " << leftor << " | " << rightor << "\n";
            std::cout << "Bit buffer after: " << bufaft << "\n";


            bit_buffer_len += bit_count;
            i += bit_count;
        }

        if(bit_buffer_len == 8) {
            //Write buffer


            std::bitset<8> buf(bit_buffer);
            std::cout << "Writing buffer to file: " << buf <<"\n";

            compressed_file.write(&bit_buffer, 1);
            bit_buffer = '\0';
            bit_buffer_len = 0;
        }
    }
}

void close_compressed_file() {
    //Write rest of buffer, and the EOF valence code if needed.
}

void write_point(MyMesh::Point point) {
    int16_t point_quant_0 = (int16_t) point[0];
    int16_t point_quant_1 = (int16_t) point[1];
    int16_t point_quant_2 = (int16_t) point[2];

    compressed_file.write((char*) &point_quant_0, 2);
    compressed_file.write((char*) &point_quant_1, 2);
    compressed_file.write((char*) &point_quant_2, 2);
}

void write_vertex(MyMesh::VertexHandle vertex) {

    int16_t point_quant_0 = (int16_t) mesh.point(vertex)[0];
    int16_t point_quant_1 = (int16_t) mesh.point(vertex)[1];
    int16_t point_quant_2 = (int16_t) mesh.point(vertex)[2];

    compressed_file.write((char*) &point_quant_0, 2);
    compressed_file.write((char*) &point_quant_1, 2);
    compressed_file.write((char*) &point_quant_2, 2);
}

void quantize_coordinates(int bits) {
    /*
    Quantize vertex coordinates to specified number of bits
    */
    int quantization_resolution = (int) pow(2, bits);

    std::cout << "Quantization resolution: " << quantization_resolution << "\n";

    //Calculate bounding box
    float bounding_box_min[3] = {FLT_MAX, FLT_MAX, FLT_MAX};
    float bounding_box_max[3] = {FLT_MIN, FLT_MIN, FLT_MIN};

    int i;

    MyMesh::VertexIter v_it, v_end(mesh.vertices_end());
    for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it) {
        MyMesh::Point v_point = mesh.point(*v_it);

        if(v_point[0] < bounding_box_min[0]) {
            bounding_box_min[0] = v_point[0];
        }
        if(v_point[1] < bounding_box_min[1]) {
            bounding_box_min[1] = v_point[1];
        }
        if(v_point[2] < bounding_box_min[2]) {
            bounding_box_min[2] = v_point[2];
        }
        
        if(v_point[0] > bounding_box_max[0]) {
            bounding_box_max[0] = v_point[0];
        }
        if(v_point[1] > bounding_box_max[1]) {
            bounding_box_max[1] = v_point[1];
        }
        if(v_point[2] > bounding_box_max[2]) {
            bounding_box_max[2] = v_point[2];
        }
    }

    // std::cout << "Bounding box:\n";
    // std::cout << "Min: " 
    //           << bounding_box_min[0] << " "
    //           << bounding_box_min[1] << " "
    //           << bounding_box_min[2] << "\n";
    // std::cout << "Max: " 
    //           << bounding_box_max[0] << " "
    //           << bounding_box_max[1] << " "
    //           << bounding_box_max[2] << "\n";
    
    //Get ranges
    float ranges[3];
    ranges[0] = bounding_box_max[0]-bounding_box_min[0];
    ranges[1] = bounding_box_max[1]-bounding_box_min[1];
    ranges[2] = bounding_box_max[2]-bounding_box_min[2];

    float max_range = 0.0f;

    for(i = 0; i < 3; i++) {
        if(ranges[i] > max_range) {
            max_range = ranges[i];
        }
    }

    //Quantize coordinates
    for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it) {
        MyMesh::Point v_point = mesh.point(*v_it);

        for(i = 0; i < 3; i++) {
            //Shift values, so the lowest coordinate is 0
            if(bounding_box_min[i] < 0.0f) {
                v_point[i] = v_point[i] + -1*bounding_box_min[i];
            }
            else {
                v_point[i] = v_point[i] - bounding_box_min[i]; 
            }
            // v_point[i] = floorf(v_point[i]);
            v_point[i] = floorf((v_point[i]/max_range)*quantization_resolution);
        }

        mesh.set_point(*v_it, v_point);
    }
}

int retriangulate_patch(MyMesh::HalfedgeHandle gate) {
    //Delete all removable patches and retriangulate
    std::vector<MyMesh::VertexHandle> face_vhandles;
    MyMesh::VertexFaceCCWIter vfccw_it;

    std::cout << "Starting retriangulation\n";

    MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(gate);
    MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(gate);
    MyMesh::VertexHandle front_vertex = mesh.opposite_vh(gate);

    int from_flag = mesh.property(v_retri_flag, from_vertex);
    int to_flag = mesh.property(v_retri_flag, to_vertex);
    int patch_degree = mesh.valence(front_vertex); 
    int i, j = 0;

    //DEBUG
    std::cout << "*Removing patch\n";
    std::cout << "Gate: " << gate << "\n";
    std::cout << from_vertex << "-" << to_vertex;
    std::cout << " " << front_vertex;
    std::cout << " TRIFLAGS: " << mesh.property(v_retri_flag, from_vertex);
    std::cout << mesh.property(v_retri_flag, to_vertex);
    std::cout  << "\n";

    //DEBUG
    if(front_vertex.idx() == -1) {
        debug_print_deque(removable_gates);
        return 0;
    }
    MyMesh::VertexHandle patch_vertices[patch_degree];

    patch_vertices[0] = from_vertex;
    patch_vertices[1] = to_vertex;

    //Find current_gate in circulator.
    MyMesh::VertexVertexCCWIter vvccw_it;
    for (vvccw_it=mesh.vv_ccwiter(front_vertex); *vvccw_it != to_vertex; ++vvccw_it) {}
    ++vvccw_it;

    i = 2;
    for (; *vvccw_it != from_vertex; ++vvccw_it) {
        patch_vertices[i] = *vvccw_it;
        i++;
    }

    //DEBUG
    // std::cout << "Patch_vertices:\n";
    // for(i = 0; i < patch_degree; i++) {
    //     std::cout << i << ": " << patch_vertices[i]
    //               << ": " << mesh.point(patch_vertices[i]) << "\n";
    // }

    //Delete faces (cannot delete while iterating)
    MyMesh::FaceHandle patch_faces[patch_degree];
    i = 0;
    for (vfccw_it=mesh.vf_ccwiter(front_vertex); vfccw_it.is_valid(); ++vfccw_it) {
        patch_faces[i] = *vfccw_it;
        i++;
    }
    for(i = 0; i < patch_degree; i++) {
        std::cout << "Deleting face " << patch_faces[i] << "\n";
        // debug_print_face(patch_faces[i]);
        mesh.delete_face(patch_faces[i]);
    }

    MyMesh::Point front_point = mesh.point(front_vertex); 
    std::cout << "Deleting vertex " << front_vertex << " " 
               << front_point <<"\n";
    //Delete vertex
    mesh.delete_vertex(front_vertex, true);

    bool reverse_retriangulation = false;
    std::vector<MyMesh::FaceHandle> added_faces;
    if(patch_degree == 3) {
        //Add 1 face
        face_vhandles.clear();
        face_vhandles.push_back(patch_vertices[0]);
        face_vhandles.push_back(patch_vertices[1]);
        face_vhandles.push_back(patch_vertices[2]);
        MyMesh::FaceHandle new_face = mesh.add_face(face_vhandles);
        mesh.property(f_state_flag, new_face) = CONQUERED; 
        // std::cout << "Adding face " << new_face <<"\n";

        added_faces.push_back(new_face);
    }
    else if(patch_degree == 4) {
        //Add two faces
        for(i = 0; i < 2; i++) {
            face_vhandles.clear();
            face_vhandles.push_back(patch_vertices[retri_conf_quad[from_flag][to_flag][i][0]]);
            face_vhandles.push_back(patch_vertices[retri_conf_quad[from_flag][to_flag][i][1]]);
            face_vhandles.push_back(patch_vertices[retri_conf_quad[from_flag][to_flag][i][2]]);
            MyMesh::FaceHandle new_face = mesh.add_face(face_vhandles);  
            
            if(new_face != MyMesh::PolyConnectivity::InvalidFaceHandle) {
                mesh.property(f_state_flag, new_face) = CONQUERED; 
                added_faces.push_back(new_face);
            }
            else {
                //Encoder cannot handle topology
                std::cout << "Concave patch\n";

                std::cout << "Tried to add face: " << face_vhandles[0] << ", " 
                          << face_vhandles[1] << ", "
                          << face_vhandles[2] << "\n" ;

                for(i = i-1; i >= 0; i--) {
                    mesh.delete_face(added_faces[i]);
                }
                reverse_retriangulation = true;
                break;

                // exit(0);
            }
            // std::cout << "Adding face " << new_face <<"\n";
        }
    }
    else if(patch_degree == 5) {

        //Check for concave patch

        //Add three faces
        for(i = 0, j = 0; i < 3; i++) {
            std::cout << "Adding face (i: " << i << ")\n";
            face_vhandles.clear();
            face_vhandles.push_back(patch_vertices[retri_conf_pent[from_flag][to_flag][i][0]]);
            face_vhandles.push_back(patch_vertices[retri_conf_pent[from_flag][to_flag][i][1]]);
            face_vhandles.push_back(patch_vertices[retri_conf_pent[from_flag][to_flag][i][2]]);
            MyMesh::FaceHandle new_face = mesh.add_face(face_vhandles);

            if(new_face != MyMesh::PolyConnectivity::InvalidFaceHandle) {
                mesh.property(f_state_flag, new_face) = CONQUERED; 
                added_faces.push_back(new_face);
            }
            else {
                //Encoder cannot handle topology
                std::cout << "Concave patch\n";


                for(i = i-1; i >= 0; i--) {
                    mesh.delete_face(added_faces[i]);
                }
                reverse_retriangulation = true;
                break;
            }
        }
    }
    else if(patch_degree == 6) {

        //Check for concave patch
        
        //Add four faces
        for(i = 0; i < 4; i++) {
            face_vhandles.clear();
            face_vhandles.push_back(patch_vertices[retri_conf_hex[from_flag][to_flag][i][0]]);
            face_vhandles.push_back(patch_vertices[retri_conf_hex[from_flag][to_flag][i][1]]);
            face_vhandles.push_back(patch_vertices[retri_conf_hex[from_flag][to_flag][i][2]]);
            MyMesh::FaceHandle new_face = mesh.add_face(face_vhandles);

            if(new_face != MyMesh::PolyConnectivity::InvalidFaceHandle) {
                mesh.property(f_state_flag, new_face) = CONQUERED; 
                added_faces.push_back(new_face);
            }
            else {
                //Encoder cannot handle topology
                std::cout << "Concave patch\n";

                for(i = i-1; i >= 0; i--) {
                    mesh.delete_face(added_faces[i]);
                }
                reverse_retriangulation = true;
                break;
            }
            // std::cout << "Adding ";   
            // debug_print_face(new_face);
        }
    }


    if(reverse_retriangulation == true) {
        std::cout << "Recreating patch\n";
        //Recreate vertex
        MyMesh::VertexHandle original_vertex = mesh.add_vertex(front_point);
        std::cout << "Creating vertex " << original_vertex << "\n";
        //Add original faces
        for(i = 0; i < patch_degree; i++) {
            MyMesh::VertexHandle next_vertex;
            if(i < patch_degree-1) {
                next_vertex = patch_vertices[i+1];
            }
            else {
                next_vertex = patch_vertices[0];
            }

            face_vhandles.clear();
            face_vhandles.push_back(patch_vertices[i]);
            face_vhandles.push_back(next_vertex);
            face_vhandles.push_back(original_vertex);
            MyMesh::FaceHandle new_face = mesh.add_face(face_vhandles);
            std::cout << "Creating face " << new_face << "\n";
        }

        return -1;


    }

    return 0;
}

// void parse_decimation_patch(MyMesh::HalfedgeHandle current_gate) {
//     /*
//     Find the next gates, and sets vertex retriangulation flags
//     */
//     MyMesh::FaceHandle face = mesh.face_handle(current_gate);
//     MyMesh::VertexHandle front_vertex = mesh.opposite_vh(current_gate);
//     uint8_t patch_degree = mesh.valence(front_vertex);

//     int from_flag = mesh.property(v_retri_flag, mesh.from_vertex_handle(current_gate));
//     int to_flag = mesh.property(v_retri_flag, mesh.to_vertex_handle(current_gate));


//     //Set neighborhood state flags
//     MyMesh::VertexVertexCCWIter vvccw_it;
//     for (vvccw_it=mesh.vv_ccwiter(front_vertex); vvccw_it.is_valid(); ++vvccw_it) {
//         mesh.property(v_state_flag, *vvccw_it) = CONQUERED;
//     }

//     //Find current_gate in circulator, and process the face adjacent to current_gate.
//     MyMesh::VertexFaceCCWIter vfccw_it;
//     for (vfccw_it=mesh.vf_ccwiter(front_vertex); *vfccw_it != face; ++vfccw_it) {}
//     ++vfccw_it;

//     //Find gates.
//     //For the gates to be pushed in the right order circulate faces
//     //in counter clockwice order starting with face adjacent to current_gate.
//     int i = 0;
//     for (; *vfccw_it != face; ++vfccw_it) {

//         //Find next gate, by finding the edge not connected to front_vertex
//         MyMesh::FaceHalfedgeIter fh_it;
//         for (fh_it=mesh.fh_iter(*vfccw_it); fh_it.is_valid(); ++fh_it) {
            
//             MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(*fh_it);
//             MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(*fh_it);
//             if(from_vertex != front_vertex &&
//                to_vertex != front_vertex) {

//                 MyMesh::HalfedgeHandle next_gate = mesh.opposite_halfedge_handle(*fh_it);

//                 //Set retriangulation flags for vertex at the end of next_gate
//                 if(mesh.property(v_retri_flag, to_vertex) != TRIINIT) {
//                     //Vertex retriangulation has already been set.
//                     //Do nothing.
//                     std::cout << "Vertex retri flags already set\n";
//                 }
//                 else if(patch_degree == 3 && i < 1) {
//                     mesh.property(v_retri_flag, to_vertex) = 
//                         retri_flag_tri[from_flag][to_flag];     
//                 }
//                 else if (patch_degree == 4 && i < 2) {
//                     mesh.property(v_retri_flag, to_vertex) = 
//                         retri_flag_quad[from_flag][to_flag][i];
//                 }
//                 else if (patch_degree == 5 && i < 3) {
//                     mesh.property(v_retri_flag, to_vertex) = 
//                         retri_flag_pent[from_flag][to_flag][i];
//                 }
//                 else if (patch_degree == 6 && i < 4) {
//                     mesh.property(v_retri_flag, to_vertex) = 
//                         retri_flag_hex[from_flag][to_flag][i];
//                 }
                
//                 std::cout << "Retri flag for " << to_vertex 
//                       << " : " << mesh.property(v_retri_flag, to_vertex) 
//                       << " (" << i << ")\n";


//                 std::cout << "Pushed gate " << next_gate << "\n";
//                 // debug_print_gate(next_gate);

//                 gate_queue.push_back(next_gate);

//                 i++;
//                 break;
//             }
//         }
//     }
// }

void parse_decimation_neighborhood(std::vector<MyMesh::VertexHandle>& neighborhood, 
                                   uint8_t patch_degree,
                                   int from_flag,
                                   int to_flag) {
    std::cout << "Decimating neighborhood\n";
    int i;
    
    //Set neighborhood state flags
    for(i = 0; i < neighborhood.size()-1; i++) {
        mesh.property(v_state_flag, neighborhood[i]) = CONQUERED;        
    }

    //Find new gates, and set retriangulation flags

    for(i = 0; i < neighborhood.size()-1; i++) {

        //Set retriangulation flags
        if(mesh.property(v_retri_flag, neighborhood[i]) != TRIINIT) {
            //Vertex retriangulation has already been set.
            //Do nothing.
           std::cout << "Vertex retri flags already set\n";
        }
        else if(patch_degree == 3) {
            mesh.property(v_retri_flag, neighborhood[i]) = 
                retri_flag_tri[from_flag][to_flag];     
        }
        else if (patch_degree == 4) {
            mesh.property(v_retri_flag, neighborhood[i]) = 
                retri_flag_quad[from_flag][to_flag][i-1];
        }
        else if (patch_degree == 5) {
            mesh.property(v_retri_flag, neighborhood[i]) = 
                retri_flag_pent[from_flag][to_flag][i-1];
        }
        else if (patch_degree == 6) {
            mesh.property(v_retri_flag, neighborhood[i]) = 
                retri_flag_hex[from_flag][to_flag][i-1];
        }
        
        // Find half-edge pointing to next vertex in neighborhood
        MyMesh::VertexOHalfedgeIter voh_it;
        for (voh_it=mesh.voh_iter(neighborhood[i]); voh_it.is_valid(); ++voh_it) {
            if(mesh.to_vertex_handle(*voh_it) == neighborhood[i+1]) {

                MyMesh::HalfedgeHandle next_gate = mesh.opposite_halfedge_handle(*voh_it);
                std::cout << "Pushed gate " << next_gate << "\n";
                // debug_print_gate(next_gate);

                gate_queue.push_back(next_gate);
                break;

            }
         }

    }
    
}

int generate_decimation_patch(MyMesh::HalfedgeHandle current_gate) {
    MyMesh::FaceHandle face = mesh.face_handle(current_gate);
    MyMesh::VertexHandle front_vertex = mesh.opposite_vh(current_gate);
    uint8_t patch_degree = mesh.valence(front_vertex);

    int from_flag = mesh.property(v_retri_flag, mesh.from_vertex_handle(current_gate));
    int to_flag = mesh.property(v_retri_flag, mesh.to_vertex_handle(current_gate));

    int i;
    std::cout << "Vertex " << front_vertex << ": patch degree " <<  (int) patch_degree <<  "\n";

    std::cout << "\nPatch info before decimation:\n";
    debug_print_patch(current_gate);

    //Find barycenter
    MyMesh::Point barycenter_sum;
    barycenter_sum[0] = barycenter_sum[1] = barycenter_sum[2] = 0.0;
    MyMesh::VertexVertexCCWIter vvccw_it;
    for (vvccw_it=mesh.vv_ccwiter(front_vertex); vvccw_it.is_valid(); ++vvccw_it) {
        barycenter_sum += mesh.point(*vvccw_it);
    }
    MyMesh::Point barycenter = barycenter_sum/patch_degree;

    //Debug
    std::cout << "Barycenter: " << barycenter << "\n";
    std::cout << "Front vertex: " << mesh.point(front_vertex) << "\n";
    MyMesh::Point bary_diff = barycenter-mesh.point(front_vertex);
    std::cout << "Difference: " << bary_diff << "\n";

    //Get face state flags, must be copied before face deletion
    std::vector<int> face_flags;
    //Find front face in cirulation
    MyMesh::VertexFaceCCWIter vfccw_it;
    for (vfccw_it=mesh.vf_ccwiter(front_vertex); *vfccw_it != face; ++vfccw_it) {}
    face_flags.push_back(mesh.property(f_state_flag,*vfccw_it));
    ++vfccw_it;
    
    for (; *vfccw_it != face; ++vfccw_it) {
        face_flags.push_back(mesh.property(f_state_flag, *vfccw_it));
    }
    std::cout << "Number of state flags: " << face_flags.size() << "\n";


    std::vector<MyMesh::VertexHandle> neighborhood;
    for (vvccw_it=mesh.vv_ccwiter(front_vertex); vvccw_it.is_valid(); ++vvccw_it) {
        neighborhood.push_back(*vvccw_it);
    }
    for(i = 0; i < neighborhood.size(); i++) {
        if(neighborhood[i] == mesh.to_vertex_handle(current_gate)) {
            std::rotate(neighborhood.begin(),neighborhood.begin()+i,neighborhood.end());
        }
    }

    //Retriangulate patch
    int retri_res = retriangulate_patch(current_gate);

    if(retri_res == -1) {
        std::cout << "Retriangulation failed\n";

        face = mesh.face_handle(current_gate);
        front_vertex = mesh.opposite_vh(current_gate);
        std::cout << face << "\n";
        std::cout << front_vertex << "\n";

        i = 0;
        for (vfccw_it=mesh.vf_ccwiter(front_vertex); *vfccw_it != face; ++vfccw_it) {}
        mesh.property(f_state_flag, *vfccw_it) = face_flags[i];
        ++vfccw_it; i++;
        
        for (; *vfccw_it != face; ++vfccw_it, ++i) {
            mesh.property(f_state_flag, *vfccw_it) = face_flags[i];
        }

        std::cout << "\nPatch info after reverted retriangulation:\n";
        debug_print_patch(current_gate);

        return -1;
    }

    //Add gates and flag neighboring vertices
    parse_decimation_neighborhood(neighborhood, patch_degree, from_flag, to_flag);

    //Write valence to output file
    compressed_file.write((char*) &patch_degree, 1);
    //Write vertex coordinates to file
    write_point(bary_diff);

    return 0;
}

void generate_cleaning_patch(MyMesh::HalfedgeHandle current_gate) {
    MyMesh::FaceHandle face = mesh.face_handle(current_gate);
    MyMesh::VertexHandle front_vertex = mesh.opposite_vh(current_gate);
    uint8_t patch_degree = mesh.valence(front_vertex);

    int from_flag = mesh.property(v_retri_flag, mesh.from_vertex_handle(current_gate));
    int to_flag = mesh.property(v_retri_flag, mesh.to_vertex_handle(current_gate));

    //Write valence to output file
    // compressed_file << patch_degree;
    compressed_file.write((char*) &patch_degree, 1);
    std::cout << "Vertex " << front_vertex << ": patch degree " <<  (int) patch_degree <<  "\n";

    mesh.property(v_state_flag, front_vertex) = REMOVABLE;

    MyMesh::Point barycenter_sum;
    barycenter_sum[0] = barycenter_sum[1] = barycenter_sum[2] = 0.0;

    //Flag vertices neighbouring front_vertex conquered.
    MyMesh::VertexVertexCCWIter vvccw_it;
    for (vvccw_it=mesh.vv_ccwiter(front_vertex); vvccw_it.is_valid(); ++vvccw_it) {
        mesh.property(v_state_flag, *vvccw_it) = CONQUERED;
        barycenter_sum += mesh.point(*vvccw_it);
    }

    //Write vertex coordinates to file
    MyMesh::Point barycenter = barycenter_sum/patch_degree;

    write_vertex(front_vertex);

    //Find current_gate in circulator, and process the face adjacent to current_gate.
    MyMesh::VertexFaceCCWIter vfccw_it;
    for (vfccw_it=mesh.vf_ccwiter(front_vertex); *vfccw_it != face; ++vfccw_it) {}
    mesh.property(f_state_flag, *vfccw_it) = REMOVABLE;
    ++vfccw_it;

    //Flag incident faces to front_vertex removable and find gates.
    //For the gates to be pushed in the right order circulate faces
    //in counter clockwice order starting with face adjacent to current_gate.
    int i = 0;
    for (; *vfccw_it != face; ++vfccw_it) {
        mesh.property(f_state_flag, *vfccw_it) = REMOVABLE;

        //Find next gate, by finding the edge not connected to front_vertex
        MyMesh::FaceHalfedgeIter fh_it;
        for (fh_it=mesh.fh_iter(*vfccw_it); fh_it.is_valid(); ++fh_it) {
            
            MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(*fh_it);
            MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(*fh_it);
            if(from_vertex != front_vertex &&
               to_vertex != front_vertex) {
                  
                MyMesh::HalfedgeHandle opposite_heh = mesh.opposite_halfedge_handle(*fh_it);

                mesh.property(v_state_flag, mesh.opposite_vh(opposite_heh)) = CONQUERED;
                std::cout << "Vertex " 
                          << mesh.opposite_vh(opposite_heh)
                          << " flagged conquered\n"; 

                //Flag adjecent face as conquered
                mesh.property(f_state_flag, 
                              mesh.face_handle(opposite_heh)) = CONQUERED;
                std::cout << "Face " 
                          << mesh.face_handle(opposite_heh) 
                          << " flagged conquered\n";

                //Add first gate
                MyMesh::HalfedgeHandle next_heh = mesh.next_halfedge_handle(opposite_heh);
                MyMesh::HalfedgeHandle next_gate = mesh.opposite_halfedge_handle(next_heh);
                gate_queue.push_back(next_gate);
                std::cout << "Pushed gate ";
                debug_print_gate(next_gate);

                //Add second gate
                next_heh = mesh.next_halfedge_handle(next_heh);
                next_gate = mesh.opposite_halfedge_handle(next_heh);
                gate_queue.push_back(next_gate);
                std::cout << "Pushed gate ";
                debug_print_gate(next_gate);

                i++;
                break;
            }
        }
    }

    //Retriangulate patch
    retriangulate_patch(current_gate);
}

void generate_null_patch(MyMesh::HalfedgeHandle current_gate) {
    MyMesh::FaceHandle face = mesh.face_handle(current_gate);
   
    mesh.property(f_state_flag, face) = CONQUERED;
    std::cout << "Null patch, creation: face " << face <<"!\n";
    char zero = 0;
    compressed_file.write(&zero, sizeof(char));
    
    MyMesh::FaceHalfedgeIter fh_it;
    for (fh_it=mesh.fh_iter(face); fh_it.is_valid(); ++fh_it) {
        if(*fh_it != current_gate) {
            MyMesh::HalfedgeHandle next_gate = mesh.opposite_halfedge_handle(*fh_it);
            gate_queue.push_back(next_gate);

            //Flag vertices as conquered if flag is not all ready set
            MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(next_gate);
            MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(next_gate);

            if (mesh.property(v_retri_flag, from_vertex) == TRIINIT) {
                mesh.property(v_retri_flag, from_vertex) = TRIADD;
            }
            if (mesh.property(v_retri_flag, to_vertex) == TRIINIT) {
                mesh.property(v_retri_flag, to_vertex) = TRISUB;
            }

            std::cout << "Null patch, pushed gate: " 
                      << mesh.from_vertex_handle(next_gate) 
                      << "-" 
                      << mesh.to_vertex_handle(next_gate) << "\n";
            debug_print_gate(next_gate);
        }
    }
}

bool is_concave_patch(MyMesh::VertexHandle front_vertex) {
    // return false;


    //Ony valence 3 cannot be concave?
    if(mesh.valence(front_vertex) < 4) {
        return false;
    }
    int i;

    std::vector<MyMesh::VertexHandle> neighborhood;

    MyMesh::VertexVertexCCWIter vvccw_it;
    for (vvccw_it=mesh.vv_ccwiter(front_vertex); vvccw_it.is_valid(); ++vvccw_it) {
        neighborhood.push_back(*vvccw_it);
    }
    neighborhood.push_back(neighborhood[0]);
    neighborhood.push_back(neighborhood[1]);

    std::vector<MyMesh::FaceHandle> patch_faces;
    MyMesh::VertexFaceCCWIter vfccw_it;
    for (vfccw_it=mesh.vf_ccwiter(front_vertex); vfccw_it.is_valid(); ++vfccw_it) {
        patch_faces.push_back(*vfccw_it);
    }
    
    // Rotate faces so that the first face contains the two first vertices.
    for(i = 0; i < patch_faces.size(); i++) {
        std::set<MyMesh::VertexHandle> face_vertices;
        MyMesh::FaceVertexCCWIter fvccw_it;
        for (fvccw_it=mesh.fv_ccwiter(patch_faces[i]); fvccw_it.is_valid(); ++fvccw_it) {
            face_vertices.insert(*fvccw_it);
        }

        if(face_vertices.count(neighborhood[0]) && face_vertices.count(neighborhood[1])) {
            std::rotate(neighborhood.begin(),
                        neighborhood.begin()+i,
                        neighborhood.end());
        }
    }
    patch_faces.push_back(patch_faces[0]); //Used to get normal of both faces

    std::cout << "Checking patch for concaveness\n";

    std::cout << "Vertices: ";
    for(i = 0; i < mesh.valence(front_vertex); i++) {
        std::cout << neighborhood[i] << " ";
    }
    std::cout << "\n";

    std::cout << "Faces: ";
    for(i = 0; i < mesh.valence(front_vertex); i++) {
        std::cout << patch_faces[i] << " ";
    }
    std::cout << "\n";

    for(i = 0; i < mesh.valence(front_vertex); i++) {
        // use the normal of the first face as reference
        // std::cout << mesh.normal(patch_faces[i]) << "\n";

        MyMesh::Point normal_1 = mesh.normal(patch_faces[i]);
        MyMesh::Point normal_2 = mesh.normal(patch_faces[i+1]);
        MyMesh::Point p1 = mesh.point(neighborhood[i]);
        MyMesh::Point p2 = mesh.point(neighborhood[i+1]);
        MyMesh::Point p3 = mesh.point(neighborhood[i+2]);
        MyMesh::Point vec1 = p2-p1;
        MyMesh::Point vec2 = p3-p2;


        float dot = vec1[0]*vec2[0] + vec1[1]*vec2[1] + vec1[2]*vec2[2];
        
        float det_1 = vec1[0]*vec2[1]*normal_1[2] + vec2[0]*normal_1[1]*vec1[2] + normal_1[0]*vec1[1]*vec2[2] -
                    vec1[2]*vec2[1]*normal_1[0] - vec2[2]*normal_1[1]*vec1[0] - normal_1[2]*vec1[1]*vec2[0];
        float angle_1 = atan2(det_1, dot);
        std::cout << "Angle from first normal:" << neighborhood[i] << "-" 
                  << neighborhood[i+1] << "-" << neighborhood[i+2] << ": " << angle_1 << "\n";

        float det_2 = vec1[0]*vec2[1]*normal_2[2] + vec2[0]*normal_2[1]*vec1[2] + normal_2[0]*vec1[1]*vec2[2] -
                    vec1[2]*vec2[1]*normal_2[0] - vec2[2]*normal_2[1]*vec1[0] - normal_2[2]*vec1[1]*vec2[0];
        float angle_2 = atan2(det_2, dot);
        std::cout << "Angle from second normal:" << neighborhood[i] << "-" 
                  << neighborhood[i+1] << "-" << neighborhood[i+2] << ": " << angle_2 << "\n";


        if(angle_1 <  0.009 || angle_2 <  0.009) {
            return true;
        } 
    }

    return false;
}


MyMesh::Point cross_product(MyMesh::Point vec1, MyMesh::Point vec2) {
    MyMesh::Point product;

    //Cross product
    product[0] = (vec1[1] * vec2[2]) - (vec1[2] * vec2[1]);
    product[1] = (vec1[2] * vec2[0]) - (vec1[0] * vec2[2]);
    product[2] = (vec1[0] * vec2[1]) - (vec1[1] * vec2[0]);

    //Normalize
    double length = sqrt(product[0]*product[0]+product[1]*product[1]+product[2]*product[2]);

    product[0] = product[0]/length;
    product[1] = product[1]/length;
    product[2] = product[2]/length;

    return product;
}

bool can_retriangulate(MyMesh::HalfedgeHandle current_gate) {
    int i, j, k;
    
    MyMesh::VertexHandle front_vertex = mesh.opposite_vh(current_gate);
    MyMesh::VertexHandle from_vertex = mesh.from_vertex_handle(current_gate);
    MyMesh::VertexHandle to_vertex = mesh.to_vertex_handle(current_gate);
    
    int patch_degree = mesh.valence(front_vertex);
    int from_flag = mesh.property(v_retri_flag, from_vertex);
    int to_flag = mesh.property(v_retri_flag, to_vertex);

    if(patch_degree == 3) {
        return true;
    }

    std::vector<MyMesh::VertexHandle> patch_vertices;

    patch_vertices.push_back(from_vertex);
    patch_vertices.push_back(to_vertex);

    //Find current_gate in circulator.
    MyMesh::VertexVertexCCWIter vvccw_it;
    for (vvccw_it=mesh.vv_ccwiter(front_vertex); *vvccw_it != to_vertex; ++vvccw_it) {}
    ++vvccw_it;

    i = 2;
    for (; *vvccw_it != from_vertex; ++vvccw_it) {
        patch_vertices.push_back(*vvccw_it);
        i++;
    }

    MyMesh::Point face_normals[patch_degree-2][2];

    std::vector<MyMesh::VertexHandle> face_vhandles;
    for(i = 0; i < patch_degree-2; i++) {
        face_vhandles.clear();
        if(patch_degree == 4) {
            face_vhandles.push_back(patch_vertices[retri_conf_quad[from_flag][to_flag][i][0]]);
            face_vhandles.push_back(patch_vertices[retri_conf_quad[from_flag][to_flag][i][1]]);
            face_vhandles.push_back(patch_vertices[retri_conf_quad[from_flag][to_flag][i][2]]);
        }
        else if(patch_degree == 5) {
            face_vhandles.push_back(patch_vertices[retri_conf_pent[from_flag][to_flag][i][0]]);
            face_vhandles.push_back(patch_vertices[retri_conf_pent[from_flag][to_flag][i][1]]);
            face_vhandles.push_back(patch_vertices[retri_conf_pent[from_flag][to_flag][i][2]]);
        }
        else if(patch_degree == 6) {
            face_vhandles.push_back(patch_vertices[retri_conf_hex[from_flag][to_flag][i][0]]);
            face_vhandles.push_back(patch_vertices[retri_conf_hex[from_flag][to_flag][i][1]]);
            face_vhandles.push_back(patch_vertices[retri_conf_hex[from_flag][to_flag][i][2]]);
        }

        std::cout << "Checking if triangle can be created:\n";
        
        //The 0-1-2 triangle        
        MyMesh::Point vec1 = mesh.point(face_vhandles[2]) - mesh.point(face_vhandles[0]);
        MyMesh::Point vec2 = mesh.point(face_vhandles[1]) - mesh.point(face_vhandles[0]);

        MyMesh::Point cross = cross_product(vec1, vec2);
        face_normals[i][0] = cross;
        std::cout << "Cross product "
                  << face_vhandles[0] << "-"
                  << face_vhandles[1] << "-"
                  << face_vhandles[2]
                  << ": " << cross << " (correct)\n";
        
        //The 0-2-1 triangle        
        vec1 = mesh.point(face_vhandles[1]) - mesh.point(face_vhandles[0]);
        vec2 = mesh.point(face_vhandles[2]) - mesh.point(face_vhandles[0]);

        cross = cross_product(vec1, vec2);
        face_normals[i][1] = cross;
        std::cout << "Cross product "
                  << face_vhandles[0] << "-"
                  << face_vhandles[2] << "-"
                  << face_vhandles[1]
                  << ": " << cross << "\n";
    }

    for(i = 0; i < patch_degree-2; i++) {

        for(j = 0; j < patch_degree-2; j++) {
            MyMesh::Point diff1 = face_normals[i][0] - face_normals[j][0];
            MyMesh::Point diff2 = face_normals[i][1] - face_normals[j][0];

            float abs_diff1 = std::abs(diff1[0]) + std::abs(diff1[1]) + std::abs(diff1[2]);
            float abs_diff2 = std::abs(diff2[0]) + std::abs(diff2[1]) + std::abs(diff2[2]);

            if(abs_diff1 > abs_diff2) {
                std::cout << "abs_diff1: " << abs_diff1 << " abs_diff2: " << abs_diff2 << "\n";
                std::cout << "CONCAVE PATCH: compared face " << i << " to " << j <<  "\n";
                return false;
            }
        }
    }
    return true;
}

void do_cleaning_conquest() {
    MyMesh::VertexVertexCCWIter vvccw_it;
    MyMesh::VertexFaceCCWIter vfccw_it;
    MyMesh::FaceHalfedgeIter fh_it;
    MyMesh::VertexOHalfedgeIter voh_it;

    //Initial steps
    //Get halfedge connected to the first vertex
    MyMesh::HalfedgeHandle init_heh;
    MyMesh::VertexHandle first_vertex = *mesh.vertices_begin();

    int lowest_idx = -1;
    for (voh_it=mesh.voh_iter(first_vertex); voh_it.is_valid(); ++voh_it) {
        if(lowest_idx == -1 ||
            mesh.to_vertex_handle(*voh_it).idx() < lowest_idx) {
            init_heh = *voh_it;
            lowest_idx = mesh.to_vertex_handle(*voh_it).idx();
        }
    }

    //Flag its vertices as conquered.
    MyMesh::VertexHandle init_v1 = mesh.from_vertex_handle(init_heh);
    MyMesh::VertexHandle init_v2 = mesh.to_vertex_handle(init_heh);
    mesh.property(v_state_flag, init_v1) = CONQUERED;
    mesh.property(v_state_flag, init_v2) = CONQUERED;

    //Set retriangulation flags
    mesh.property(v_retri_flag, init_v1) = TRISUB;
    mesh.property(v_retri_flag, init_v2) = TRIADD;

    gate_queue.push_back(init_heh);

    //DEBUG
    int d_break = 0;
    //Process gates in gate_queue, conquest is finised when gate_queue is empty.
    while(!gate_queue.empty()) {
        std::cout << "\nd_break c: " << d_break << "\n";
        MyMesh::HalfedgeHandle current_gate = gate_queue.front();
        //Remove current_gate from gate_queue
        gate_queue.pop_front();

        MyMesh::FaceHandle face = mesh.face_handle(current_gate);
        MyMesh::VertexHandle front_vertex = mesh.opposite_vh(current_gate);
        int front_vertex_valence = mesh.valence(front_vertex);

        if(mesh.property(f_state_flag, face) == CONQUERED ||
           mesh.property(f_state_flag, face) == REMOVABLE) {
            //Nothing to do.

            //DEBUG
            if (mesh.property(f_state_flag, face) == CONQUERED) {
                std::cout << "Ignoring conquered face: " << face << "\n";
            }
            else if(mesh.property(f_state_flag, face) == REMOVABLE) {
                std::cout << "Ignoring removable face: " << face << "\n";
            }
        }
        else if(mesh.property(v_state_flag, front_vertex) == FREE &&
                front_vertex_valence == 3) {
            
            generate_cleaning_patch(current_gate);
        }
        else {
            //Front face is a null patch
            generate_null_patch(current_gate);
        }

        std::cout << "Finished working on gate(" << current_gate << ") ";
        std::cout << mesh.from_vertex_handle(current_gate) 
                  << "-"
                  << mesh.to_vertex_handle(current_gate)
                  << " front:" << mesh.opposite_vh(current_gate)
                  << " face:" << face;
        std::cout << " retriflags: " 
                  << mesh.property(v_retri_flag, mesh.from_vertex_handle(current_gate))
                  << mesh.property(v_retri_flag, mesh.to_vertex_handle(current_gate))
                  << "\n\n";


        // DEBUG
        // if(d_break==1) {
            // break;
        // }
        d_break++;
    }

     // Delete all elements that are marked as deleted by OpenMesh.
    mesh.garbage_collection();

    debug_print_deque(gate_queue);
}

void do_conquest() {
    MyMesh::VertexVertexCCWIter vvccw_it;
    MyMesh::VertexFaceCCWIter vfccw_it;
    MyMesh::FaceHalfedgeIter fh_it;
    MyMesh::VertexOHalfedgeIter voh_it;

    //Initial steps
    //Get halfedge connected to the first vertex
    MyMesh::HalfedgeHandle init_heh;
    MyMesh::VertexHandle first_vertex = *mesh.vertices_begin();

    int lowest_idx = -1;
    for (voh_it=mesh.voh_iter(first_vertex); voh_it.is_valid(); ++voh_it) {
        if(lowest_idx == -1 ||
            mesh.to_vertex_handle(*voh_it).idx() < lowest_idx) {
            init_heh = *voh_it;
            lowest_idx = mesh.to_vertex_handle(*voh_it).idx();
        }
    }

    //Flag its vertices as conquered.
    MyMesh::VertexHandle init_v1 = mesh.from_vertex_handle(init_heh);
    MyMesh::VertexHandle init_v2 = mesh.to_vertex_handle(init_heh);
    mesh.property(v_state_flag, init_v1) = CONQUERED;
    mesh.property(v_state_flag, init_v2) = CONQUERED;

    //Set retriangulation flags
    mesh.property(v_retri_flag, init_v1) = TRISUB;
    mesh.property(v_retri_flag, init_v2) = TRIADD;

    gate_queue.push_back(init_heh);
    // std::cout << "init_heh: " << init_heh 
    //           << " " << mesh.from_vertex_handle(init_heh)
    //           << "-" << mesh.to_vertex_handle(init_heh)
    //           << "\n";

    // std::cout << init_v1 << init_v2 << init_front_vertex << "\n";

    int failed_decimations = 0;
    int concave_patches = 0;
    int vertices_removed = 0;

    //DEBUG
    int d_break = 0;
    //Process gates in gate_queue, conquest is finised when gate_queue is empty.
    while(!gate_queue.empty()) {
        std::cout << "\nd_break g: " << d_break << "\n";
        MyMesh::HalfedgeHandle current_gate = gate_queue.front();
        //Remove current_gate from gate_queue
        std::cout << "Popping gate\n";
        gate_queue.pop_front();

        MyMesh::FaceHandle face = mesh.face_handle(current_gate);
        MyMesh::VertexHandle front_vertex = mesh.opposite_vh(current_gate);
        int front_vertex_valence = mesh.valence(front_vertex);

        if(mesh.property(f_state_flag, face) == CONQUERED ||
           mesh.property(f_state_flag, face) == REMOVABLE) {
            //Nothing to do.

            //DEBUG
            if (mesh.property(f_state_flag, face) == CONQUERED) {
                std::cout << "Ignoring conquered face: " << face << "\n";
            }
            else if(mesh.property(f_state_flag, face) == REMOVABLE) {
                std::cout << "Ignoring removable face: " << face << "\n";
            }
        }
        else if(mesh.property(v_state_flag, front_vertex) == FREE &&
                front_vertex_valence <= 6) {

            //Check for patch concaveness
            if(is_concave_patch(front_vertex)) {
                std::cout << "Found concave patch: cannot decimate\n";
                concave_patches++;
                generate_null_patch(current_gate);
            }
            else {
                //Found a new patch
                int res = generate_decimation_patch(current_gate);

                if(res == -1) {
                    //Could not decimate patch, create null patch instead
                    std::cout << "Decimation of patch failed\n";
                    failed_decimations++;
                    generate_null_patch(current_gate);
                }
                else {
                    vertices_removed++;
                }
            }
        }
        else if(mesh.property(v_state_flag, front_vertex) == CONQUERED ||
                (mesh.property(v_state_flag, front_vertex) == FREE &&
                 front_vertex_valence > 6)) {
            //Front face is a null patch
            generate_null_patch(current_gate);

        }

        std::cout << "Finished working on gate(" << current_gate << ") ";
        std::cout << mesh.from_vertex_handle(current_gate) 
                  << "-"
                  << mesh.to_vertex_handle(current_gate)
                  << " front:" << mesh.opposite_vh(current_gate)
                  << " face:" << face;
        std::cout << " retriflags: " 
                  << mesh.property(v_retri_flag, mesh.from_vertex_handle(current_gate))
                  << mesh.property(v_retri_flag, mesh.to_vertex_handle(current_gate))
                  << "\n";

        // debug_print_deque(gate_queue);

        // DEBUG
        if(d_break == 1) {
            // break;
        }
        d_break++;
    }

    std::cout << "Finished conquest\n";

    std::cout << "Number of failed decimations: " << failed_decimations << "\n";
    std::cout << "Number of skipped concave patches: " << concave_patches << "\n";
    std::cout << "Vertices_removed: " << vertices_removed << "\n";

    // Delete all elements that are marked as deleted by OpenMesh.
    mesh.garbage_collection();

    std::cout << "Garbage collected\n";

    // debug_print_deque(gate_queue);
} 

void initialize_mesh_data() {
    std::cout << "Initializing vertex data.\n";
    MyMesh::VertexIter v_it, v_end(mesh.vertices_end());
    for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it) {
        //Set triangulation flag to 3
        mesh.property(v_retri_flag, *v_it) = TRIINIT;
        mesh.property(v_state_flag, *v_it) = FREE;
    }

    std::cout << "Initializing face data.\n";
    MyMesh::FaceIter f_it, f_end(mesh.faces_end());
    for (f_it=mesh.faces_begin(); f_it!=f_end; ++f_it) {
        mesh.property(f_state_flag, *f_it) = FREE;
    } 

    gate_queue.clear();   
}

void print_mesh_data() {
    std::cout << "Vertices:\n";
    MyMesh::VertexIter v_it, v_end(mesh.vertices_end());
    for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it) {
        std::cout << *v_it << " " << mesh.property(v_state_flag, *v_it) << "\n";
    }
    std::cout << "Faces:\n";
    MyMesh::FaceIter f_it, f_end(mesh.faces_end());
    for (f_it=mesh.faces_begin(); f_it!=f_end; ++f_it) {
        std::cout << *f_it << " " << mesh.property(f_state_flag, *f_it) << "\n";
    }
}

int main(int argc, char* argv[]) {
    /*
    Usage:

    prog_valence input_mesh output_mesh


    */

    //Add the required properties
    mesh.add_property(f_state_flag);
    mesh.add_property(h_state_flag);
    mesh.add_property(v_state_flag);
    mesh.add_property(v_retri_flag);

    //Enabled deletion of mesh items
    mesh.request_face_status();
    mesh.request_edge_status();
    mesh.request_vertex_status();

    mesh.request_face_normals();

    // Read input mesh
    if (!OpenMesh::IO::read_mesh(mesh, argv[1])) 
    {
        std::cerr << "read error\n";
        exit(1);
    }

    // std::cout << "Mesh supports\n";
    // MESHOPT("vertex normals", mesh.has_vertex_normals());
    // MESHOPT("vertex colors", mesh.has_vertex_colors());
    // MESHOPT("texcoords", mesh.has_vertex_texcoords2D());
    // MESHOPT("face normals", mesh.has_face_normals());
    // MESHOPT("face colors", mesh.has_face_colors());

    quantize_coordinates(10);


    mesh.update_normals(); //Must be updated before each iteration

    compressed_file.open("coded.txt");
    initialize_mesh_data();
    do_conquest();
    compressed_file.close();
    
    compressed_file.open("coded_cleaning.txt");
    initialize_mesh_data();
    do_cleaning_conquest();
    compressed_file.close();    


    std::cout << "Writing mesh to file\n";
    
    // CHKWOPT(FaceColor);
    // Write output mesh
    if (!OpenMesh::IO::write_mesh(mesh, argv[2])) 
    {
        std::cerr << "write error\n";
        exit(1);
    }


    return 0;
}